﻿
using WebApplication1.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography;
using System.Text;



namespace WebApplication1.Controllers
{
    public class HomeController : Controller
        
        
    {
        private static int countPosts = 10;
        private BilliardEntities db = new BilliardEntities();
        public ActionResult Index()
        {
            
            IEnumerable<News> news = db.News;
            ViewBag.News = news;
            IEnumerable<Files> files = db.Files;
            ViewBag.Files = files;

            return View();
        }

        public ActionResult PersonalOffice()
        {
            IEnumerable<Users> users = db.Users;
            ViewBag.Users = users;
            IEnumerable<Role> role = db.Role;
            ViewBag.Role = role;
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            SelectList files = new SelectList(db.Files, "file_id", "file_name");
            ViewBag.Files = files;

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(News add)
        {
            SelectList files = new SelectList(db.Files, "file_id", "file_name");
            ViewBag.Files = files;

            if (ModelState.IsValid)
            {
                News newss = null;
                newss = db.News.FirstOrDefault(u => u.title_news == add.title_news);
                IEnumerable<News> news = db.News;
                ViewBag.News = news;



                if (newss == null)
                {

                    db.News.Add(new News { id_news = add.id_news, title_news = add.title_news, name_news = add.name_news, date_news = add.date_news, author_news = add.author_news,file_id=add.file_id});
                    db.SaveChanges();

                    ModelState.AddModelError("", "Новина успішно додана");
                }
                else
                    ModelState.AddModelError("", "Новина  уже існує");
            }

            return View();
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            News news = db.News.FirstOrDefault(u => u.id_news == id);
            SelectList files = new SelectList(db.Files, "file_id", "file_name");
            ViewBag.Files = files;
            if (news == null)
                return View("Error");

            return View(news);
        }


        [HttpPost]
        public ActionResult Delete(int? id)
        {
            News news = db.News.FirstOrDefault(u => u.id_news == id);
            SelectList files = new SelectList(db.Files, "file_id", "file_name");
            ViewBag.Files = files;
            if (news != null)
            {
                db.News.Remove(news);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
                ModelState.AddModelError("", "Новина не знайдена");

            return View("");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            SelectList files = new SelectList(db.Files, "file_id", "file_name");
            ViewBag.Files = files;
            IEnumerable<News> news = db.News;
            ViewBag.News = news;
      
            News newss = db.News.FirstOrDefault(u => u.id_news == id);

            if (newss == null)
                return View("Error");

            return View(newss);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, News edit)
        {
            SelectList files = new SelectList(db.Files, "file_id", "file_name");
            ViewBag.Files = files;
            IEnumerable<News> news = db.News;
            ViewBag.News = news;
            
                News newss = db.News.FirstOrDefault(u => u.id_news == id);
            if (newss != null)
            {
                if (ModelState.IsValid)
                {
                newss.name_news = edit.name_news;
                newss.title_news = edit.title_news;
                newss.date_news = edit.date_news;
                newss.author_news = edit.author_news;
                    newss.file_id = edit.file_id;

                    db.SaveChanges();
                    ModelState.AddModelError("", "Зміни успішно збережені");
                }
            }
            else
                ModelState.AddModelError("", "Новина не знайдена");

            return View(newss);
        }

        [Route("files")]
        public ActionResult Files(string search, string sort, int? pageNumber)
        {
            var files = db.Files.Where(f => f.file_name.Contains(search) || search == null);

            ViewBag.SortByName = string.IsNullOrEmpty(sort) ? "name" : "";

            switch (sort)
            {
                case "name":
                    files = files.OrderBy(o => o.file_name);
                    break;
                default:
                    break;
            }

            return View(files.ToList().ToPagedList(pageNumber ?? 1, countPosts));
        }

        [Route("files/add")]
       
        [HttpGet]
        public ActionResult FileAdd()
        {
            return View();
        }

        [Route("files/add")]
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FileAdd(HttpPostedFileBase upload, string fileName)
        {
            string type = "." + upload.ContentType.Split('/')[1];

            Files file = db.Files.FirstOrDefault(u => u.file_name == fileName);

            if (file == null)
            {
                string resultPath = "/Content/images/" + fileName + type;
                upload.SaveAs(Server.MapPath("~" + resultPath));

                db.Files.Add(new Files { file_path = resultPath, file_name = fileName });
                db.SaveChanges();

                ModelState.AddModelError("", "Файл успешно завантежений");
            }
            else
                ModelState.AddModelError("", "Файл с таким ім'ям вже існує");

            return View();
        }


        [Route("files/delete/{id:int}")]
      
        [HttpGet]
        public ActionResult FileDelete(int id)
        {
            Files file = db.Files.FirstOrDefault(u => u.file_id == id);
            var path = AppDomain.CurrentDomain.BaseDirectory;

            if (file == null || !System.IO.File.Exists(path + file.file_path))
                return View("Error");

            return View(file);
        }

        [Route("files/delete/{id:int}")]
      
        [HttpPost]
        public ActionResult FileDelete(int? id)
        {
            Files file = db.Files.FirstOrDefault(u => u.file_id == id);

            var path = AppDomain.CurrentDomain.BaseDirectory;

            if (System.IO.File.Exists(path + file.file_path) || file == null)
            {
                db.Files.Remove(file);
                db.SaveChanges();

                System.IO.File.Delete(path + file.file_path);

                return RedirectToAction("Files");
            }
            else
                ModelState.AddModelError("", "Файл не знайден");

            return View();
        }

    }
}