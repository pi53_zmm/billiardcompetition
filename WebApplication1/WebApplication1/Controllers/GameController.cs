﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{

    
    public class GameController : Controller
    {
        BilliardEntities db = new BilliardEntities();
        
        // GET: Game

        public ActionResult Game(int Id)
        {

            IEnumerable<Game> game = db.Game;
            ViewBag.Game = game;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Competition> competition = db.Competition;
  
            ViewBag.Competition = competition;
            ViewBag.IdCompetition = Id;
         
            return View(Id);
        }
       
       
        [HttpGet]
        public ActionResult Create(int IdCompetition)
        {
            
            IEnumerable<Game> game = db.Game;
            ViewBag.Game = game;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            return View(IdCompetition);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Game add,int IdCompetition)
        {
            
           
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            
            if (ModelState.IsValid)
            {
                Game games = null;
                games = db.Game.FirstOrDefault(u => u.id_game == add.id_game);
                IEnumerable<Game> game = db.Game;
                ViewBag.Game = game;
                if (games == null)
                {
                    db.Game.Add(new Game { id_game = add.id_game, name_game = add.name_game, game_status = add.game_status, sportsman_id_1=add.sportsman_id_1,sportsman_id_2=add.sportsman_id_2,score_1= add.score_1});
                    db.SaveChanges();
                    ModelState.AddModelError("", "Гра успішно додана");
                }
                else
                    ModelState.AddModelError("", "Гра вже існує");
            }
            return View(IdCompetition);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Game game = db.Game.FirstOrDefault(u => u.id_game == id);
            if (game == null)
                return View("Error");
            return View(game);
        }


        [HttpPost]
        public ActionResult Delete(int? id)
        {
            Game game = db.Game.FirstOrDefault(u => u.id_game == id);
            if (game != null)
            {
                db.Game.Remove(game);
                db.SaveChanges();
                return RedirectToAction("Game");
            }
            else
                ModelState.AddModelError("", "Гра не знайдена");

            return View("");
        }
        [HttpGet]
        public ActionResult Edit(int id,int IdCompetition)
        {
            IEnumerable<Game> game = db.Game;
            ViewBag.Game = game; IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            Game games = db.Game.FirstOrDefault(u => u.id_game == id);
            ViewBag.IdCompetition = IdCompetition;
            ViewBag.Id = id;
            if (games == null)
                return View("Error");
            return View(games);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Game edit, Game add, int IdCompetition)
        {
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Game> game = db.Game;
            ViewBag.Game = game;
            ViewBag.Id = id;

            Game games = db.Game.FirstOrDefault(u => u.id_game == id);
            ViewBag.IdCompetition = IdCompetition;
            if (games != null)
            {
                if (ModelState.IsValid)
                {
                  
                    games.score_1 = edit.score_1;
                    games.score_2 = edit.score_2;
                    db.SaveChanges();
                    ModelState.AddModelError("", "Зміни збереженно");
                }
            }
            else
                ModelState.AddModelError("", "Гра не знайдена");

            IEnumerable<Competition_sportsman> Competition_sportsman = db.Competition_sportsman;
            int GameCount = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).Count();

            string NameGame = String.Format("1/{0}", GameCount / 2);
            Game score = db.Game.FirstOrDefault(u => u.id_game == id);
            Game Conect1 = db.Game.FirstOrDefault(u => u.conection1 == id);
            Game Conect2 = db.Game.FirstOrDefault(u => u.conection2 == id);





            /////////////////////////////////////////
            if (score.score_1 > score.score_2)
            {
                if (Conect1 != null)
                {
                    Conect1.sportsman_id_1 = score.sportsman_id_1;
                }
                if (Conect2 != null)
                {
                    Conect2.sportsman_id_2 = score.sportsman_id_1;
                }
                Competition_sportsman competition_Sportsman = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.id_sportsman == score.sportsman_id_2);
                Sportsman sportsmans = db.Sportsman.FirstOrDefault(u => u.id_sportsman == competition_Sportsman.id_sportsman);
               
                if (competition_Sportsman.id_sportsman == score.sportsman_id_2)
                {
                    int k = 2;
                    int point = 5;
                    for (int i = 0; i < GameCount / 2; i++)
                    {
                       
                        if (score.name_game == NameGame)
                        {
                            competition_Sportsman.result_sportsman = (GameCount / k) + 1;
                            competition_Sportsman.points = point;
                            
                            break;
                        }
                        k = k + 2;
                        point = point + 5;
                        NameGame = String.Format("1/{0}", GameCount / k);
                    }

                    ////////////////////////////////
                    int CountGameSportsman1 = db.Game.Where(u => u.sportsman_id_1 == score.sportsman_id_2).Where(p => p.id_competition == IdCompetition).Count();
                    int CountGameSportsman2 = db.Game.Where(u => u.sportsman_id_2 == score.sportsman_id_2).Where(p => p.id_competition == IdCompetition).Count();
                    int count = CountGameSportsman1 + CountGameSportsman2;
                    List<int> GameSportsman1 = new List<int>();
                    var iDGameSportsman1 = db.Game.Where(p => p.id_competition == IdCompetition).Where(u => u.sportsman_id_1 == score.sportsman_id_2).ToArray();
                    var iDGameSportsman2 = db.Game.Where(p => p.id_competition == IdCompetition).Where(u => u.sportsman_id_2 == score.sportsman_id_2).ToArray();//ошибка
                    foreach (var item in iDGameSportsman1)
                    {
                        GameSportsman1.Add(item.id_game);
                    }
                    foreach (var item in iDGameSportsman2)
                    {
                        GameSportsman1.Add(item.id_game);
                    }
                    int max = 0;
                    int scoreSportsman1 = 0;
                    int scoreSportsman2 = 0;
                    int winGame = 0;
                    
                 
                    for (int i = 0; i < count; i++)

                    {
                        int v = GameSportsman1[i];
                        Game pro = db.Game.FirstOrDefault(p => p.id_game == v);
                        
                        
                            if (competition_Sportsman.id_sportsman == pro.sportsman_id_1)
                            {
                                if (pro.score_1 > pro.score_2)
                                {
                                    
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_1);
                                scoreSportsman2=scoreSportsman2+ Convert.ToInt16(pro.score_2);
                                max = max + scoreSportsman1 + scoreSportsman2;
                                winGame++;
                            }
                                else
                                {
                                    
                                scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_2);
                                scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_1);
                                max = max + scoreSportsman1 + scoreSportsman2;
                            }
                            }
                            else
                            {
                            if (pro.score_1 > pro.score_2)
                            {
                                
                                scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_2);
                                scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_1);
                                max = max + scoreSportsman1 + scoreSportsman2;
                            }
                            else
                            {
                               
                                scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_2);
                                scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_1);
                                max = max + scoreSportsman1 + scoreSportsman2;
                                winGame++;
                            }
                        }
                        
                    }
                   double performans = (scoreSportsman1*100) / max;
                    winGame = (winGame * 100) / count;
                    
                        competition_Sportsman.performances = Math.Round((performans + winGame) / 2, 2);
                 
                    db.SaveChanges();
                    /////////////////////////////////////////
                    if (competition_Sportsman.result_sportsman == 2)
                    {
                        Competition_sportsman winer = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.id_sportsman == score.sportsman_id_1);
                        Sportsman sportsman_winer = db.Sportsman.FirstOrDefault(u => u.id_sportsman == score.sportsman_id_1);
                        Competition competitions = db.Competition.FirstOrDefault(p => p.id_competition == IdCompetition);
                        winer.result_sportsman = 1;
                        winer.points = point + 5;
                        

                        int CountGameSportsmanwin = db.Game.Where(u => u.sportsman_id_1 == score.sportsman_id_1).Where(p => p.id_competition == IdCompetition).Count();
                        int CountGameSportsmanwin2 = db.Game.Where(u => u.sportsman_id_2 == score.sportsman_id_1).Where(p => p.id_competition == IdCompetition).Count();
                        int countwin = CountGameSportsmanwin + CountGameSportsmanwin2;
                        List<int> GameSportsmanwin = new List<int>();
                        var iDGameSportsmanwin1 = db.Game.Where(p => p.id_competition == IdCompetition).Where(u => u.sportsman_id_1 == score.sportsman_id_1).ToArray();
                        var iDGameSportsmanwin2 = db.Game.Where(p => p.id_competition == IdCompetition).Where(u => u.sportsman_id_2 == score.sportsman_id_1).ToArray();//ошибка
                        foreach (var item in iDGameSportsmanwin1)
                        {
                            GameSportsmanwin.Add(item.id_game);
                        }
                        foreach (var item in iDGameSportsmanwin2)
                        {
                            GameSportsmanwin.Add(item.id_game);
                        }
                         max = 0;
                         scoreSportsman1 = 0;
                        scoreSportsman2 = 0;
                        winGame = 0;


                        for (int i = 0; i < countwin; i++)

                        {
                            int v = GameSportsmanwin[i];
                            Game pro = db.Game.FirstOrDefault(p => p.id_game == v);


                            if (winer.id_sportsman == pro.sportsman_id_1)
                            {
                                if (pro.score_1 > pro.score_2)
                                {
                                    
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_1);
                                    scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_2);
                                    max = max + scoreSportsman1 + scoreSportsman2;
                                    winGame++;
                                }
                                else
                                {
                                    
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_1);
                                    scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_2);
                                    max = max + scoreSportsman1 + scoreSportsman2;
                                }
                            }
                            else
                            {
                                if (pro.score_1 > pro.score_2)
                                {
                                  
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_2);
                                    scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_1);
                                    max = max + scoreSportsman1 + scoreSportsman2;
                                }
                                else
                                {
                                    
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_2);
                                    scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_1);
                                    max = max + scoreSportsman1 + scoreSportsman2;
                                    winGame++;
                                }
                            }

                        }
                        double performanswin = (scoreSportsman1 * 100) / max;
                        
                        winGame = (winGame * 100) / countwin;
                        
                            winer.performances = Math.Round((performanswin + winGame) / 2, 2);
                        db.SaveChanges();
                        ///добаление performants и points в таблицу sportsman
                        ///



                        //////Редактировать
                        List<int> ids = new List<int>();
                        var idSportsman = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).ToArray();
                        foreach (var item in idSportsman)
                        {
                            ids.Add(Convert.ToInt16(item.id_sportsman));
                        }

                        for (int i = 0; i < ids.Count; i++)
                        {
                            int j = ids[i];
                            Sportsman sportsmanPerfomance = db.Sportsman.FirstOrDefault(u => u.id_sportsman == j);
                            Competition_sportsman competition_ = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.id_sportsman == j);
                            sportsmanPerfomance.count_competition = sportsmanPerfomance.count_competition + 1;
                            sportsmanPerfomance.SumPerformances = sportsmanPerfomance.SumPerformances + competition_.performances;
                            sportsmanPerfomance.performance = Math.Round(Convert.ToDouble((sportsmanPerfomance.SumPerformances) / sportsmanPerfomance.count_competition), 2);
                            sportsmanPerfomance.point = sportsmanPerfomance.point + competition_.points;
                            competitions.competiton_status = true;
                            db.SaveChanges();
                        }

                    }
                }

                db.SaveChanges();
            }




            else
            {
                if (Conect1 != null)
                {
                    Conect1.sportsman_id_1 = score.sportsman_id_2;
                }
                if (Conect2 != null)
                {
                    Conect2.sportsman_id_2 = score.sportsman_id_2;
                }
                Competition_sportsman competition_Sportsman = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.id_sportsman == score.sportsman_id_1);
                Sportsman sportsmans = db.Sportsman.FirstOrDefault(u => u.id_sportsman == competition_Sportsman.id_sportsman);
                if (competition_Sportsman.id_sportsman == score.sportsman_id_1)
                {
                    int k = 2;
                    int point = 5;
                    for (int i = 0;i<GameCount/2 ; i++)
                    {
                        if (score.name_game == NameGame)
                        {
                            competition_Sportsman.result_sportsman =(GameCount/k)+1 ;
                            competition_Sportsman.points = point;
                            
                            
                            break;
                        }
                        point = point + 5;
                        k = k + 2;
                        NameGame = String.Format("1/{0}", GameCount / k);
                    }


                    int CountGameSportsman1 = db.Game.Where(u => u.sportsman_id_1 == score.sportsman_id_1).Where(p => p.id_competition == IdCompetition).Count();
                    int CountGameSportsman2 = db.Game.Where(u => u.sportsman_id_2 == score.sportsman_id_1).Where(p => p.id_competition == IdCompetition).Count();//1
                    int count = CountGameSportsman1 + CountGameSportsman2;
                    List<int> GameSportsman1 = new List<int>();
                    var iDGameSportsman1 = db.Game.Where(p => p.id_competition == IdCompetition).Where(u => u.sportsman_id_1 == score.sportsman_id_1).ToArray();
                    var iDGameSportsman2 = db.Game.Where(p => p.id_competition == IdCompetition).Where(u => u.sportsman_id_2 == score.sportsman_id_1).ToArray();//ошибка
                    foreach (var item in iDGameSportsman1)
                    {
                        GameSportsman1.Add(item.id_game);
                    }
                    foreach (var item in iDGameSportsman2)
                    {
                        GameSportsman1.Add(item.id_game);
                    }
                    int max = 0;
                    int scoreSportsman1 = 0;
                    int scoreSportsman2 = 0;
                    int winGame = 0;


                    for (int i = 0; i < count; i++)

                    {
                        int v = GameSportsman1[i];
                        Game pro = db.Game.FirstOrDefault(p => p.id_game == v);


                        if (competition_Sportsman.id_sportsman == pro.sportsman_id_1)
                        {
                            if (pro.score_1 > pro.score_2)
                            {
                                
                                scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_1);
                                scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_2);
                                max = max + scoreSportsman1 + scoreSportsman2;
                                winGame++;
                            }
                            else
                            {
                                
                                scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_1);
                                scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_2);
                                max = max + scoreSportsman1 + scoreSportsman2;
                            }
                        }
                        else
                        {
                            if (pro.score_1 > pro.score_2)
                            {
                                
                                scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_2);
                                scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_1);
                                max = max + scoreSportsman1 + scoreSportsman2;
                            }
                            else
                            {
                                
                                scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_2);
                                scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_1);
                                max = max + scoreSportsman1 + scoreSportsman2;
                                winGame++;
                            }
                        }

                    }
                    double performans = (scoreSportsman1 * 100) / max;
                    winGame = (winGame * 100) / count;
                    competition_Sportsman.performances = Math.Round((performans + winGame) / 2, 2);
                    db.SaveChanges();



                    if (competition_Sportsman.result_sportsman == 2)
                    {
                        Competition_sportsman winer = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.id_sportsman == score.sportsman_id_2);
                        Sportsman sportsman_winer=db.Sportsman.FirstOrDefault(u => u.id_sportsman == score.sportsman_id_2);
                        Competition competitions=db.Competition.FirstOrDefault(p => p.id_competition == IdCompetition);
                        winer.result_sportsman = 1;
                        winer.points = point + 5;
                        

                        int count1=0;
                        int CountGameSportsmanwin = db.Game.Where(u => u.sportsman_id_1 == score.sportsman_id_1).Where(p => p.id_competition == IdCompetition).Count();
                        int CountGameSportsmanwin2 = db.Game.Where(u => u.sportsman_id_2 == score.sportsman_id_1).Where(p => p.id_competition == IdCompetition).Count();
                        count1 = CountGameSportsmanwin + CountGameSportsmanwin2;
                        List<int> GameSportsmanwin = new List<int>();
                        var iDGameSportsmanwin1 = db.Game.Where(p => p.id_competition == IdCompetition).Where(u => u.sportsman_id_1 == score.sportsman_id_2).ToArray();
                        var iDGameSportsmanwin2 = db.Game.Where(p => p.id_competition == IdCompetition).Where(u => u.sportsman_id_2 == score.sportsman_id_2).ToArray();//ошибка
                        foreach (var item in iDGameSportsmanwin1)
                        {
                            GameSportsmanwin.Add(item.id_game);
                        }
                        foreach (var item in iDGameSportsmanwin2)
                        {
                            GameSportsmanwin.Add(item.id_game);
                        }
                        max = 0;
                        scoreSportsman1 = 0;
                        scoreSportsman2 = 0;
                        winGame = 0;


                        for (int i = 0; i < count1; i++)

                        {
                            int v = GameSportsmanwin[i];
                            Game pro = db.Game.FirstOrDefault(p => p.id_game == v);


                            if (winer.id_sportsman == pro.sportsman_id_1)
                            {
                                if (pro.score_1 > pro.score_2)
                                {
                                  
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_1);
                                    scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_2);
                                    max = max + scoreSportsman1 + scoreSportsman2;
                                    winGame++;
                                }
                                else
                                {
                                    
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_1);
                                    scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_2);
                                    max = max + scoreSportsman1 + scoreSportsman2;
                                }
                            }
                            else
                            {
                                if (pro.score_1 > pro.score_2)
                                {
                                    
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_2);
                                    scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_1);
                                    max = max + scoreSportsman1 + scoreSportsman2;
                                }
                                else
                                {
                                    
                                    scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(pro.score_2);
                                    scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(pro.score_1);
                                    max = max + scoreSportsman1 + scoreSportsman2;
                                    winGame++;
                                }
                            }

                        }
                        double performanswin = (scoreSportsman1 * 100) / max;
                        winGame = (winGame * 100) / count1;
                        winer.performances = Math.Round((performanswin + winGame) / 2, 2);
                        db.SaveChanges();

                        ///добаление performants и points в таблицу sportsman
                        if (competitions.competiton_status == null)
                        {
                            List<int> ids = new List<int>();
                            var idSportsman = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).ToArray();
                            foreach (var item in idSportsman)
                            {
                                ids.Add(Convert.ToInt16(item.id_sportsman));
                            }

                            for (int i = 0; i < ids.Count; i++)
                            {
                                int j = ids[i];
                                Sportsman sportsmanPerfomance = db.Sportsman.FirstOrDefault(u => u.id_sportsman == j);
                                Competition_sportsman competition_ = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.id_sportsman == j);
                                sportsmanPerfomance.count_competition = sportsmanPerfomance.count_competition + 1;
                                sportsmanPerfomance.SumPerformances = sportsmanPerfomance.SumPerformances + competition_.performances;
                                sportsmanPerfomance.performance = Math.Round(Convert.ToDouble((sportsmanPerfomance.SumPerformances) / sportsmanPerfomance.count_competition), 2);
                                sportsmanPerfomance.point =sportsmanPerfomance.point+ competition_.points;
                                competitions.competiton_status = true;
                                db.SaveChanges();
                            }
                        }

                    }
                }

                db.SaveChanges();  
            }


            return View(games);
        }
    }
}