﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1;
using WebApplication1.Models;
using System.Data.Entity;
using System.Web.Security;
using WebApplication1.Providers;


namespace WebApplication1.Controllers
{
   
    public class AccountController : Controller
    {
        //  BilliardEntities db = new BilliardEntities();
        // GET: Account
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            
           
            if (ModelState.IsValid)
            {
                // поиск пользователя в бд
                Users user = null;
                using (BilliardEntities db = new BilliardEntities())
                {
                    user = db.Users.FirstOrDefault(u => u.email_user == model.email && u.password_user == model.password);
                    

                }
                if (user != null)
                {

                    
                        FormsAuthentication.SetAuthCookie(model.email, true);
                   // if (User.Identity.IsAuthenticated)
                        return RedirectToAction("PersonalOffice", "Home");
                  
                }
                else
                {
                    ModelState.AddModelError("", "Користувача з таким логіном та паролем не існує");
                }
            }

            return View(model);
        }
        public ActionResult Register()
        {
            //IEnumerable<Role> role = db.Role;
            //.Role = role;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            
            
           // IEnumerable<Role> role = db.Role;
           // ViewBag.Role = role;
            if (ModelState.IsValid)
            {

                Users user = null;
                using (BilliardEntities db = new BilliardEntities())
                {
                    user = db.Users.FirstOrDefault(u => u.email_user == model.email);
                }
                if (user == null)
                {
                    // создаем нового пользователя
                    using (BilliardEntities db = new BilliardEntities())
                    {

                        db.Users.Add(new Users { name_user = model.name, email_user = model.email, password_user = model.password, id_role = 3});//role=user
                            user = db.Users.Where(u => u.email_user == model.email && u.password_user == model.password).FirstOrDefault();
                            db.SaveChanges();



                        }

                    // если пользователь удачно добавлен в бд
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(model.email, true);
                       
                    }
                    return RedirectToAction("Login", "Account");

                }
                else
                {
                    ModelState.AddModelError("", "Користувач з таким логіном вже існує");
                }

            }

            return View(model);
        }
        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}