﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    

    public class PrognosticationController : Controller
    {
        BilliardEntities db = new BilliardEntities();
        // GET: Prognostication
        public ActionResult PrognosticationMain()
        {
            IEnumerable<Prognostication> prognostication = db.Prognostication;
            ViewBag.Prognostication = prognostication.OrderBy(u => u.id_competition);
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            IEnumerable<Discipline> discipline = db.Discipline;
            ViewBag.Discipline = discipline;
            return View();
        }
            public ActionResult Prognostication(int IdCompetition)
        {
            IEnumerable<Prognostication> prognostication = db.Prognostication;
            ViewBag.IdCompetition = IdCompetition;
            ViewBag.Prognostication = prognostication.OrderBy(u=>u.id_competition);
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            IEnumerable<Game> game = db.Game;
            ViewBag.Game = game;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            List<int> ids = new List<int>();
            var idGame = db.Game.ToArray();
            foreach (var item in idGame)
            {
                ids.Add(item.id_game);
            }
           
            int max = 0;
            int scoreSportsman1 = 0;
            int scoreSportsman2 = 0;
            for (int i = 0; i < ids.Count; i++)
            {

                int id = ids[i];
                
                max = 0;
                Prognostication prognostications = db.Prognostication.FirstOrDefault(u => u.id_game == id);
                Game games = db.Game.FirstOrDefault(u => u.id_game == id);
                if (prognostications == null)
                {
                    if (games.sportsman_id_1 != null && games.sportsman_id_2 != null)
                    {
                        Sportsman sportsman1 = db.Sportsman.FirstOrDefault(u => u.id_sportsman == games.sportsman_id_1);
                        Sportsman sportsman2 = db.Sportsman.FirstOrDefault(u => u.id_sportsman == games.sportsman_id_2);
                        List<int> idGames = new List<int>();
                        for (int j = 0; j < ids.Count; j++)
                        {
                            int id1 = ids[j];
                            Game gameproc = db.Game.FirstOrDefault(u => u.id_game == id1);
                            if ((gameproc.sportsman_id_1 == sportsman1.id_sportsman && gameproc.sportsman_id_2 == sportsman2.id_sportsman) || (gameproc.sportsman_id_2 == sportsman1.id_sportsman && gameproc.sportsman_id_1 == sportsman2.id_sportsman))
                            {
                                if (sportsman1.id_sportsman == gameproc.sportsman_id_1)
                                {
                                    if (gameproc.score_1 > gameproc.score_2)
                                    {
                                        
                                        scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(gameproc.score_1);
                                        scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(gameproc.score_2);
                                        max = max + scoreSportsman1 + scoreSportsman2;

                                    }
                                    else
                                    {
                                        
                                        scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(gameproc.score_1);
                                        scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(gameproc.score_2);
                                        max = max + scoreSportsman1 + scoreSportsman2;
                                    }
                                }
                                else
                                {
                                    if (gameproc.score_1 > gameproc.score_2)
                                    {
                                       
                                        scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(gameproc.score_2);
                                        scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(gameproc.score_1);
                                        max = max + scoreSportsman1 + scoreSportsman2;
                                    }
                                    else
                                    {
                                        
                                        scoreSportsman1 = scoreSportsman1 + Convert.ToInt16(gameproc.score_2);
                                        scoreSportsman2 = scoreSportsman2 + Convert.ToInt16(gameproc.score_1);
                                        max = max + scoreSportsman1 + scoreSportsman2;

                                    }
                                }

                            }
                        }
                        double perform1=0;
                        double perform2=0;
                        if (max != 0)
                        {
                            perform1 = (scoreSportsman1 * 100) / max;
                            perform2 = (scoreSportsman2 * 100) / max;
                        }
                        
                        

                       
                        
                        db.SaveChanges();
                        double percent_sportsman_1 = 50;
                        double percent_sportsman_2 = 50;
                        double difference = 0;


                        double percen1 = (Convert.ToDouble(sportsman1.performance) * 0.3) + (Convert.ToDouble(sportsman1.point) * 0.2) + (perform1 * 0.5);
                        double percen2 = (Convert.ToDouble(sportsman2.performance) * 0.3) + (Convert.ToDouble(sportsman2.point) * 0.2) + (perform2 * 0.5);
                        if (percen1 > percen2)
                        {
                            difference = ((percen1 - percen2) / percen1) * 100;
                            difference = difference / 2;
                            percent_sportsman_1 = Math.Round((percent_sportsman_1 + difference), 2);
                            percent_sportsman_2 = Math.Round((percent_sportsman_2 - difference), 2);

                        }
                        else if (percen1 < percen2)
                        {
                            difference = ((percen2 - percen1) / percen2) * 100;
                            difference = difference / 2;
                            percent_sportsman_1 = Math.Round((percent_sportsman_1 - difference), 2);
                            percent_sportsman_2 = Math.Round((percent_sportsman_2 + difference), 2);
                        }
                        db.Prognostication.Add(new Prognostication { id_game = games.id_game, percent_sportsman_1 = percent_sportsman_1, percent_sportsman_2 = percent_sportsman_2, sportsman_id_1 = games.sportsman_id_1, sportsman_id_2 = games.sportsman_id_2,id_competition=games.id_competition });
                        db.SaveChanges();
                    }
                }
            }
            return View();
        }
      
    }

}