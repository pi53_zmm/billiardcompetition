﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Globalization;
using System.Data.SqlClient;

namespace WebApplication1.Controllers
{
    
    public class UserController : Controller
    {
        BilliardEntities db = new BilliardEntities();
        // GET: User
        public ActionResult Users()
        {
            IEnumerable<Users> users = db.Users;
            ViewBag.Users = users;
            IEnumerable<Role> role = db.Role;
            ViewBag.Role = role;
            return View();
        }
        [HttpGet]
        public ActionResult Create()
        {
            IEnumerable<Role> role = db.Role;
            ViewBag.Role = role;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Users add)
        {
            IEnumerable<Role> role = db.Role;
            ViewBag.Role = role;
            if (ModelState.IsValid)
            {
                Users users = null;
                users = db.Users.FirstOrDefault(u => u.id_user == add.id_user);
                IEnumerable<Users> user = db.Users;
                ViewBag.Users = user;

                if (users == null)
                {
                    db.Users.Add(new Users { id_user = add.id_user,name_user=add.name_user, email_user = add.email_user, password_user = add.password_user,id_role=add.id_role });
                    db.SaveChanges();

                    ModelState.AddModelError("", "Користувач доданий");
                }
                else
                    ModelState.AddModelError("", "Користувач вже існує");
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            Users user = db.Users.FirstOrDefault(u => u.id_user == id);

            if (user == null)
                return View("Error");

            return View(user);
        }


        [HttpPost]
        public ActionResult Delete(int? id)
        {
            Users user = db.Users.FirstOrDefault(u => u.id_user == id);

            if (user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();

                return RedirectToAction("Users");
            }
            else
                ModelState.AddModelError("", "Користувач не знайдений");

            return View("");
        }
        public ActionResult DeleteAccount(int id)
        {
            Users user = db.Users.FirstOrDefault(u => u.id_user == id);

            if (user == null)
                return View("Error");

            return View(user);
        }


        [HttpPost]
        public ActionResult DeleteAccount(int? id)
        {
            Users user = db.Users.FirstOrDefault(u => u.id_user == id);

            if (user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
                ModelState.AddModelError("", "Користувач не знайдений");

            return View("");
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            IEnumerable<Role> role = db.Role;
            ViewBag.Role = role;
            IEnumerable<Users> users = db.Users;
            ViewBag.Users = users;
            Users user = db.Users.FirstOrDefault(u => u.id_user == id);
            ViewBag.Id = id;

            if (user == null)
                return View("Error");

            return View(user);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Users edit)
        {
            IEnumerable<Role> role = db.Role;
            ViewBag.Role = role;
            IEnumerable<Users> users = db.Users;
            ViewBag.Users = users;
            Users user = db.Users.FirstOrDefault(u => u.id_user == id);
            if (user != null)
            {
                if (ModelState.IsValid)
                {
                    
                    
                    
                    user.id_role = edit.id_role;



                    db.SaveChanges();
                    ModelState.AddModelError("", "Зміни успішно збереженні");
                }
            }
            else
                ModelState.AddModelError("", "Змінни не можливі");

            return View(user);
        }
        [HttpGet]
        public ActionResult EditUsers(int id)
        {
            IEnumerable<Role> role = db.Role;
            ViewBag.Role = role;
            Users user = db.Users.FirstOrDefault(u => u.id_user == id);

            if (user == null)
                return View("Error");

            return View(user);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUsers(int id, Users edit)
        {
            IEnumerable<Role> role = db.Role;
            ViewBag.Role = role;
            Users user = db.Users.FirstOrDefault(u => u.id_user == id);
            if (user != null)
            {
                if (ModelState.IsValid)
                {
                    user.id_user = edit.id_user;
                    user.name_user = edit.name_user;
                    user.email_user = edit.email_user;
                    user.password_user = edit.password_user;
                    user.id_role = edit.id_role;



                    db.SaveChanges();
                    ModelState.AddModelError("", "Зміни успішно збереженні");
                }
            }
            else
                ModelState.AddModelError("", "Змінни не можливі");

            return View(user);
        }
    }
}