﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.UI;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class CompetitionController : Controller
    {
        BilliardEntities db = new BilliardEntities();
        // GET: Competition
        int Size = 0;
        
        public ActionResult Grid(int IdCompetition)
        {

            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            ViewBag.IdCompetition = IdCompetition;
            return View();
        }

        public ActionResult Regulations(int IdCompetition)
        {
            Bundle jqmobile = new ScriptBundle("~/bundle/jquerymobile")
               .Include("~/Scripts/jquery-{version}.js",
                   "~/Scripts/jquery.mobile-{version}.js");

            Bundle jqmobileCSS =
                new StyleBundle("~/bundle/jquerymobileCSS")
                    .Include("~/Content/jquery.mobile-{version}.css");

            IEnumerable<Competition> competition = db.Competition;
            ViewBag.competition = competition;
            IEnumerable<Discipline> discipline = db.Discipline;
            ViewBag.Discipline = discipline;
            ViewBag.IdCompetition = IdCompetition;
            return View();
        }
        public ActionResult Games(int IdCompetition)
        {
            Bundle jqmobile = new ScriptBundle("~/bundle/jquerymobile")
               .Include("~/Scripts/jquery-{version}.js",
                   "~/Scripts/jquery.mobile-{version}.js");

            Bundle jqmobileCSS =
                new StyleBundle("~/bundle/jquerymobileCSS")
                    .Include("~/Content/jquery.mobile-{version}.css");

            IEnumerable<Competition> competition = db.Competition;
            ViewBag.competition = competition;
            IEnumerable<Discipline> discipline = db.Discipline;
            ViewBag.Discipline = discipline;
            ViewBag.IdCompetition = IdCompetition;
            return View();
        }

        public ActionResult ResultSportsman(int IdCompetition)
        {
            Bundle jqmobile = new ScriptBundle("~/bundle/jquerymobile")
               .Include("~/Scripts/jquery-{version}.js",
                   "~/Scripts/jquery.mobile-{version}.js");

            Bundle jqmobileCSS =
                new StyleBundle("~/bundle/jquerymobileCSS")
                    .Include("~/Content/jquery.mobile-{version}.css");
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Ranks> ranks = db.Ranks;
            ViewBag.Ranks = ranks;
            IEnumerable<Competition_sportsman> competition_Sportsman = db.Competition_sportsman;
            ViewBag.CompetitionSportsman = competition_Sportsman.OrderByDescending(u=>u.points);
            IEnumerable<Game> game = db.Game;
            ViewBag.Game = game;
            ViewBag.IdCompetition = IdCompetition;
            return View();
        }

        public ActionResult Competitions()
        {
            
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.competition = competition.OrderBy(u=>u.competiton_status);
            IEnumerable<Discipline> discipline = db.Discipline;
            ViewBag.Discipline = discipline;

            return View();
        }
        
        public ActionResult Create()
        {
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.competition = competition;
            IEnumerable<Discipline> discipline = db.Discipline;
            ViewBag.Discipline = discipline;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult Create(Competition add)
        {
            
            IEnumerable<Discipline> discipline = db.Discipline;
            ViewBag.Discipline = discipline;
            if (ModelState.IsValid)
            {
                Competition competition = null;
                competition = db.Competition.FirstOrDefault(u => u.name_competition == add.name_competition);
                IEnumerable<Competition> competitions = db.Competition;
                ViewBag.Competition = competitions;



                if (competition == null)
                {
                    db.Competition.Add(new Competition { id_competition = add.id_competition, name_competition = add.name_competition, date_start=add.date_start,date_end=add.date_end,id_discipline=add.id_discipline,country_competition=add.country_competition,city_competition=add.city_competition, competiton_status=add.competiton_status, regulation=add.regulation  });
                    db.SaveChanges();

                    ModelState.AddModelError("", "Змагання успішно додано");
                }
                else
                    ModelState.AddModelError("", "Змагання з такою назвою вже існує");
            }

            return View();
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            Competition competitions = db.Competition.FirstOrDefault(u => u.id_competition == id);
            IEnumerable<Discipline> discipline = db.Discipline;
            ViewBag.Discipline = discipline;
            if (competitions == null)
                return View("Error");

            return View(competitions);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Competition edit)
        {
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            IEnumerable<Discipline> discipline = db.Discipline;
            ViewBag.Discipline = discipline;
            Competition competitions = db.Competition.FirstOrDefault(u => u.id_competition == id);
            if (competitions != null)
            {
                if (ModelState.IsValid)
                {
                    
                    competitions.name_competition = edit.name_competition;
                    competitions.date_start = edit.date_start;
                    competitions.date_end = edit.date_end;
                    competitions.id_discipline = edit.id_discipline;
                    competitions.country_competition = edit.country_competition;
                    competitions.city_competition = edit.city_competition;
                    competitions.regulation = edit.regulation;
                    
                    db.SaveChanges();
                    ModelState.AddModelError("", "Зміни збереженно");
                }
            }
            else
                ModelState.AddModelError("", "Змагання не знайдено");

            return View(competitions);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Competition competition = db.Competition.FirstOrDefault(u => u.id_competition == id);

            if (competition == null)
                return View("Error");

            return View(competition);
        }


        [HttpPost]
        public ActionResult Delete(int? id)
        {
            Competition competition = db.Competition.FirstOrDefault(u => u.id_competition == id);

            if (competition != null)
            {
                db.Competition.Remove(competition);
                db.SaveChanges();

                return RedirectToAction("Competitions");
            }
            else
                ModelState.AddModelError("", "Змагання не знайдено");

            return View("");
        }

        public ActionResult CompetitionSportsman(int IdCompetition)
        {
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.Competition = competition;
            
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Ranks> ranks = db.Ranks;
            ViewBag.Ranks = ranks;
            IEnumerable<Competition_sportsman> competition_Sportsman = db.Competition_sportsman;
            ViewBag.CompetitionSportsman = competition_Sportsman;
            Competition_sportsman competition_Sportsmans= db.Competition_sportsman.Where(u => u.id_competition == IdCompetition).FirstOrDefault(u => u.toss!=null);
            ViewBag.Competition_sportsmans = competition_Sportsmans;
            IEnumerable<Game> game = db.Game;
            ViewBag.Game = game;
            ViewBag.IdCompetition = IdCompetition;
            

            return View(IdCompetition);
        }
        public ActionResult AddSportsman(int IdCompetition)
        {
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.competition = competition;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Competition_sportsman> competition_Sportsman = db.Competition_sportsman;
            ViewBag.Competition_sportsman = competition_Sportsman;
            ViewBag.IdCompetition = IdCompetition;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSportsman(Competition_sportsman add,int IdCompetition,Sportsman edit)
        {
            
            IEnumerable<Competition> competition = db.Competition;
            ViewBag.competition = competition;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            ViewBag.IdCompetition = IdCompetition;
            if (ModelState.IsValid)
            {
                Competition_sportsman competition_sportsman = null;
                competition_sportsman = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.id_sportsman == add.id_sportsman);
                Competition_sportsman competition_Sportsmans = db.Competition_sportsman.FirstOrDefault(u => u.id_competition == IdCompetition);
                IEnumerable<Competition_sportsman> competition_Sportsman = db.Competition_sportsman;
                ViewBag.Competition_sportsman = competition_Sportsman;
                Sportsman sportsmans=db.Sportsman.FirstOrDefault(u => u.id_sportsman == add.id_sportsman);


               
                    if (competition_sportsman == null)
                    {
                        db.Competition_sportsman.Add(new Competition_sportsman { id_competition_sportsman = add.id_competition_sportsman, id_competition = IdCompetition, id_sportsman = add.id_sportsman });
                        db.SaveChanges();

                        ModelState.AddModelError("", "Спортсмен успішно доданий");
                        Size++;
                    }
                    else
                        ModelState.AddModelError("", "Спортсмен з таким ім'ям уже існує");
               
            }

            return View();
        }

        [HttpGet]
        public ActionResult DeleteSportsman(int id)
        {
            Competition_sportsman competitionSportsman = db.Competition_sportsman.FirstOrDefault(u => u.id_competition_sportsman == id);
            ViewBag.IdCompetition = competitionSportsman.id_competition;
            if (competitionSportsman == null)
                return View("Error");

            return View(competitionSportsman);
        }


        [HttpPost]
        public ActionResult DeleteSportsman(int? id)
        {
            Competition_sportsman competitionSportsman = db.Competition_sportsman.FirstOrDefault(u => u.id_competition_sportsman == id);
            ViewBag.IdCompetition = competitionSportsman.id_competition;
            if (competitionSportsman != null)
            {
                db.Competition_sportsman.Remove(competitionSportsman);
                db.SaveChanges();

                return RedirectToAction("Спортсмена видалено");
            }
            else
                ModelState.AddModelError("", "Спортсмена не знайдено");

            return View("");
        }

        [HttpPost]
        public void EditCompetitionSportsman(int IdCompetition, Competition_sportsman edit, Game add)

        {

            IEnumerable<Competition_sportsman> competition_Sportsman = db.Competition_sportsman;
            //  ViewBag.CompetitionSportsman = competition_Sportsman;

            int SportsmanCount = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).Count();
            ViewBag.SportsmanCount = SportsmanCount;
            
            int toss2 = SportsmanCount;
            int toss1 = 1;

            SportsmanCount++;
            int[] RandomToss = new int[SportsmanCount];
            Random r = new Random();
            int i = 0;
            while (i < SportsmanCount)
            {

                int temp = r.Next(1, SportsmanCount);

                if (RandomToss[temp] == 0)
                {
                    RandomToss[temp] = i;

                    ++i;
                }
            }
            for (int j = 0; j < SportsmanCount; j++)
            {
                Competition_sportsman competition_sportsman = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.toss == null);
                if (competition_sportsman != null)
                {

                    if (ModelState.IsValid)
                    {

                        competition_sportsman.toss = RandomToss[j + 1];
                        db.SaveChanges();

                    }
                }

            }
            


            /////Созданиие игр

            Game games = null;
            games = db.Game.FirstOrDefault(u => u.id_game == add.id_game);
            IEnumerable<Game> game = db.Game;
            ViewBag.Game = game;
            string NameGame = String.Format("1/{0}", SportsmanCount / 2);
            // var CountId;
            if (games == null)
            {
                for (int q = 0; q < SportsmanCount / 2; q++)
                {
                    Competition_sportsman competition_sportsman1 = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.toss == toss1);
                    Competition_sportsman competition_sportsman2 = db.Competition_sportsman.Where(p => p.id_competition == IdCompetition).FirstOrDefault(u => u.toss == toss2);
                    if (ModelState.IsValid)
                    {
                        db.Game.Add(new Game { id_game = add.id_game, name_game = NameGame, sportsman_id_1 = competition_sportsman1.id_sportsman, sportsman_id_2 = competition_sportsman2.id_sportsman, id_competition = IdCompetition });
                        //добавить в список id//  CountId.Add();
                        db.SaveChanges();

                    }
                    toss1++;
                    toss2--;
                }


            }
            
            Response.Redirect("~/Competition/CompetitionSportsman?IdCompetition=" + IdCompetition);
            ///Cоздания остальних стадий с связями с предедущими
            List<int> ids = new List<int>();
            var idGame = db.Game.Where(p => p.id_competition == IdCompetition).ToArray();
            foreach(var item in idGame)
            {
                ids.Add(item.id_game);
            }
                    
                    int CountGame = (SportsmanCount / 2)/2;
            int w = 0;
            int GameCount = db.Game.Where(p => p.id_competition == IdCompetition).Count();
           

            while (GameCount <= SportsmanCount-1 )
            {
                NameGame = String.Format("1/{0}", CountGame);
                for (int q = 0; q < CountGame; q++)
                {

                    
                    db.Game.Add(new Game { id_game = add.id_game, name_game = NameGame, id_competition = IdCompetition, conection1 = ids[w], conection2 = ids[w + 1] });
                    db.SaveChanges();
                    w = w + 2;
                }

                GameCount = db.Game.Where(p => p.id_competition == IdCompetition).Count();
                
                CountGame = CountGame / 2;
                var iDGame = db.Game.Where(p => p.id_competition == IdCompetition).Where(u=>u.name_game==NameGame).ToArray();
                foreach (var item in iDGame)
                {
                    ids.Add(item.id_game);
                }
            }
            
        }
    }
}