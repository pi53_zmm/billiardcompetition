﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Data.SqlClient;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
   


    public class SportsmanController : Controller
    {


        BilliardEntities db = new BilliardEntities();

        // GET: Sportsman
        public ActionResult Sportsman()
        {

            

            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.sportsman = sportsman.OrderBy(u=>u.name_sportsman);
            IEnumerable<Ranks> ranks = db.Ranks;
            ViewBag.Ranks = ranks;



            return View();

        }

        public ActionResult Reiting(Sportsman edit)
        {
            IEnumerable<Sportsman> sportsman = db.Sportsman;
     
            ViewBag.sportsman = sportsman.OrderByDescending(u => u.point).OrderByDescending(k=>k.performance);
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            IEnumerable<Ranks> ranks = db.Ranks;
            ViewBag.Ranks = ranks;
            return View();
        }
        
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Sportsman add)
        {
            IEnumerable<Ranks> ranks = db.Ranks;
            ViewBag.Ranks = ranks;

            if (ModelState.IsValid)
            {
                Sportsman sportsmans = null;
                sportsmans = db.Sportsman.FirstOrDefault(u => u.name_sportsman == add.name_sportsman);
                IEnumerable<Sportsman> sportsman = db.Sportsman;
                ViewBag.Sportsman = sportsman;
                


                if (sportsmans == null)
                {

                    db.Sportsman.Add(new Sportsman { id_sportsman = add.id_sportsman, name_sportsman = add.name_sportsman, birthday_sportsman = add.birthday_sportsman, id_rank = add.id_rank, country_sportsman = add.country_sportsman, city_sportsman = add.city_sportsman, count_competition = 0, performance = 0,point=0,rating=0,SumPerformances=0 });
                    db.SaveChanges();

                    ModelState.AddModelError("", "Спортсмен успішно доданий");
                }
                else
                    ModelState.AddModelError("", "Спортсмен с таким ім'ям уже існує");
            }

            return View();
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Sportsman sportsman = db.Sportsman.FirstOrDefault(u => u.id_sportsman == id);

            if (sportsman == null)
                return View("Error");

            return View(sportsman);
        }


        [HttpPost]
        public ActionResult Delete(int? id)
        {
            Sportsman sportsman = db.Sportsman.FirstOrDefault(u => u.id_sportsman == id);

            if (sportsman != null)
            {
                db.Sportsman.Remove(sportsman);
                db.SaveChanges();

                return RedirectToAction("Sportsman");
            }
            else
                ModelState.AddModelError("", "Работа не найдена");

            return View("");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            SelectList files = new SelectList(db.Files, "file_id", "file_name");
            ViewBag.Files = files;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Ranks> ranks = db.Ranks;
            ViewBag.Ranks = ranks;
            Sportsman sportsmans = db.Sportsman.FirstOrDefault(u => u.id_sportsman == id);

            if (sportsmans == null)
                return View("Error");

            return View(sportsmans);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Sportsman edit)
        {
            SelectList files = new SelectList(db.Files, "file_id", "file_name");
            ViewBag.Files = files;
            IEnumerable<Sportsman> sportsman = db.Sportsman;
            ViewBag.Sportsman = sportsman;
            IEnumerable<Ranks> ranks = db.Ranks;
            ViewBag.Ranks = ranks;
            Sportsman sportsmans = db.Sportsman.FirstOrDefault(u => u.id_sportsman == id);
            if (sportsmans != null)
            {
                if (ModelState.IsValid)
                {
                    sportsmans.id_sportsman = edit.id_sportsman;
                    sportsmans.name_sportsman = edit.name_sportsman;
                    sportsmans.birthday_sportsman = edit.birthday_sportsman;
                    
                    sportsmans.country_sportsman = edit.country_sportsman;



                    db.SaveChanges();
                    ModelState.AddModelError("", "Зміни успішно збережені");
                }
            }
            else
                ModelState.AddModelError("", "Спортсмен не знайден");

            return View(sportsmans);
        }
    }
    
}
