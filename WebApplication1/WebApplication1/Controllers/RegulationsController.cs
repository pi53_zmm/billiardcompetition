﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class RegulationsController : Controller
    {
        // GET: Regulations
        public ActionResult Regulations()

        {

            return View();
        }
        public ActionResult PyramidRules()

        {

            return View();
        }
        public ActionResult StraightCannon ()

        {

            return View();
        }
        public ActionResult SimpleChips()

        {

            return View();
        }
        public ActionResult FiveChips()

        {

            return View();
        }
        public ActionResult SingleBreastedCannon()

        {

            return View();
        }
        public ActionResult ThreeBreastedCannon()

        {

            return View();
        }
        public ActionResult FreePyramid()

        {

            return View();
        }
        public ActionResult CombinedPyramid()

        {

            return View();
        }
        public ActionResult DynamicPyramid()

        {

            return View();
        }
        public ActionResult ClassicPyramidFiftyOne()

        {

            return View();
        }
        public ActionResult ClassicPyramidEight()

        {

            return View();
        }
        public ActionResult ClassicPyramidSeventyOne()

        {

            return View();
        }
        public ActionResult PyramidTeamCompetitions()

        {

            return View();
        }
        public ActionResult GeneralPoolRules()

        {

            return View();
        }
        public ActionResult PoolOfEight()

        {

            return View();
        }
        public ActionResult PoolOfNine()

        {

            return View();
        }
        public ActionResult PoolfFourteenPlusOne()

        {

            return View();
        }
        public ActionResult Snooker()

        {

            return View();
        }
    }
}