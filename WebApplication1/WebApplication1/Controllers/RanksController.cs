﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class RanksController : Controller
    {
        BilliardEntities db = new BilliardEntities();
        // GET: Ranks
        public ActionResult Rank()
        {
            IEnumerable<Ranks> ranks = db.Ranks;
            ViewBag.Ranks = ranks;
            return View();
        }
    }
}