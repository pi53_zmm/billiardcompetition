﻿

namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    

    [Table("Roles")]
    public partial class Roles
    {
        public Roles()
        {

        }

        [Key]
        public int id_role { get; set; }
        [Required]
        [StringLength(120)]
        public string name_role { get; set; }

    }
   
}