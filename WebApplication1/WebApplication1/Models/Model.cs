﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models

{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class RegisterModel
    {
        [Required]
        public string name { get; set; }
        [EmailAddress]
        [Required]
        public string email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage = "Пароли не совпадают")]
        public string Confirmpassword { get; set; }
       


    }
    public class LoginModel
    {
        [Required]
        public string email{ get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }
    }

    public partial class Newsss
    {
        [Key]
        public string name { get; set; }
        
        public int id { get; set; }
       
        public string title { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string author { get; set; }
       
        public Nullable<int> file { get; set; }

        public virtual Files Files { get; set; }
    }
    

    }