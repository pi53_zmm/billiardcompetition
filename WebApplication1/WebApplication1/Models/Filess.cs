﻿//namespace WebApplication1
//{
//    using System;
//    using System.Collections.Generic;
//    using System.ComponentModel.DataAnnotations;
//    using System.ComponentModel.DataAnnotations.Schema;
//    using System.Data.Entity.Spatial;

//    public partial class Files
//    {
//        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
//        public Files()
//        {
//            this.News = new HashSet<News>();
//            this.Sportsman = new HashSet<Sportsman>();
//        }

//        [Key]
//        public int file_id { get; set; }

//        [Required]
//        [StringLength(200)]
//        public string file_path { get; set; }

//        [StringLength(100)]
//        public string file_name { get; set; }

//        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
//        public virtual ICollection<News> News { get; set; }
//        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
//        public virtual ICollection<Sportsman> Sportsman { get; set; }
//    }
//}
