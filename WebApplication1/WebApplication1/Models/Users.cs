﻿namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Users
    {
        [Key]
        public int id_users { get; set; }

        [EmailAddress]
        [Required]
        [StringLength(120)]
        public string email { get; set; }

        [Required]
        [StringLength(50)]
        public string passwords { get; set; }

        public int? id_role { get; set; }

        public virtual Roles Roles { get; set; }

    }
}