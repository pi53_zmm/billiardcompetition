﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class Newss : Controller
    {
        public class Picture
        {
            public int id_news { get; set; }
            public string title_news { get; set; }
            public string name_news { get; set; }
            public Nullable<System.DateTime> date_news { get; set; }
            public string author_news { get; set; }
            public Nullable<int> file_id { get; set; }

            public virtual Files Files { get; set; }
        }


    }
}