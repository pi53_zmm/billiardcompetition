//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1
{
    using System;
    using System.Collections.Generic;
    
    public partial class Competition_sportsman
    {
        public int id_competition_sportsman { get; set; }
        public Nullable<int> result_sportsman { get; set; }
        public Nullable<int> id_competition { get; set; }
        public Nullable<int> id_sportsman { get; set; }
        public Nullable<int> toss { get; set; }
        public Nullable<double> performances { get; set; }
        public Nullable<double> points { get; set; }
    
        public virtual Competition Competition { get; set; }
        public virtual Sportsman Sportsman { get; set; }
    }
}
