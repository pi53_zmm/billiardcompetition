create database Billiard
use Billiard 

Create table News(
id_news int identity(1,1) not null primary key,
title_news varchar(250),
name_news text,
date_news date,
author_news varchar(120),
file_id int, foreign key (file_id) references Files(file_id)
)
Select * from News
drop table News

Create table Role(
id_role int identity(1,1) not null primary key,
name_role varchar(20)
)

insert into Role(name_role) values ('admin')
insert into Role(name_role) values ('moderator')
insert into Role(name_role) values ('user')

select * from Role
Delete Role
Where id_role=1004

Create table Users (
id_user int identity(1,1) not null primary key,
name_user varchar(50) not null,
email_user varchar(50) not null,
password_user varchar(50) not null,
id_role int foreign key(id_role) references Role(id_role)
)

INSERT INTO Users(name_user,email_user,password_user,id_role) VALUES('Zaya','tiger62613@gmail.com','12345','1')

Select * from Users

Create table Ranks(
id_rank int identity(1,1) not null primary key,
name_rank varchar(50)
)
Select * from Ranks
drop table Ranks
INSERT INTO Ranks(name_rank) VALUES('')
INSERT INTO Ranks(name_rank) VALUES('1 ������')
INSERT INTO Ranks(name_rank) VALUES('2 ������')
INSERT INTO Ranks(name_rank) VALUES('3 ������')
INSERT INTO Ranks(name_rank) VALUES('���')
INSERT INTO Ranks(name_rank) VALUES('��')
INSERT INTO Ranks(name_rank) VALUES('����')
INSERT INTO Ranks(name_rank) VALUES('���')
DELETE FROM Ranks

Create table Sportsman(
id_sportsman int identity(1,1) not null primary key,
name_sportsman varchar(120) not null,
birthday_sportsman date not null,
id_rank int foreign key(id_rank) references Ranks(id_rank), 
country_sportsman varchar(50) not null,
city_sportsman varchar(50) not null,
count_competition int,
performance float,
SumPerformances float,
rating int,
point float,
file_id int,
foreign key (file_id) references Files(file_id)
)


Select * from Sportsman
drop table Sportsman

Create table Game(
id_game int identity(1,1) not null primary key,
name_game varchar(100),
game_status bit default null,
sportsman_id_1 int,
sportsman_id_2 int,

score_1 int  default 0,
score_2 int  default 0,
conection1 int,
conection2 int,
id_competition int,
)
select * from Game
drop table Game
TRUNCATE TABLE Sportsman
TRUNCATE TABLE Competition_sportsman
TRUNCATE TABLE Game

Create table Discipline(
id_discipline int identity(1,1) not null primary key,
name_discipline varchar(20)
)
INSERT INTO Discipline(name_discipline) VALUES('³���� ������')
INSERT INTO Discipline(name_discipline) VALUES('�������� ������')
INSERT INTO Discipline(name_discipline) VALUES('���������� ������')
drop table Discipline
Select * from Discipline

Create table Competition(
id_competition int identity(1,1) not null primary key,
name_competition varchar(50),
date_start date not null,
date_end date not null,
id_discipline int foreign key(id_discipline) references Discipline(id_discipline),
country_competition varchar(50),
city_competition varchar(50),
regulation text,
competiton_status bit default null
)
drop table Competition
Select *from Competition
Delete Competition
Where id_competition =1

Create table Competition_sportsman(
id_competition_sportsman int identity(1,1) not null primary key,
result_sportsman int,
id_competition int foreign key(id_competition) references Competition(id_competition),
id_sportsman int foreign key(id_sportsman) references Sportsman(id_sportsman),
toss int,
performances float,
points float
)

create table Files(
file_id int identity(1, 1) primary key,
file_path varchar(200),
file_name varchar(100)
)
select * from Files
drop table Files

Select *from Competition_sportsman

drop table Competition_sportsman

drop table Competition
update Competition_sportsman 
set	toss=null

Create table Prognostication(
id_prognostication int identity(1,1) not null primary key,
id_game int foreign key(id_game) references Game(id_game),
sportsman_id_1 int,
sportsman_id_2 int,
id_competition int,
percent_sportsman_1 float,
percent_sportsman_2 float
)

select * from Prognostication
drop table Prognostication

 

SELECT * FROM INFORMATION_SCHEMA.TABLES

SELECT  CAST(CURRENT_TIMESTAMP AS DATE)

create procedure create_backup
 @path varchar(200)
as
begin
	BACKUP DATABASE Billiard
	TO DISK = @path
	WITH INIT, NAME = 'Zaichenko Full Db backup',
	DESCRIPTION = 'Zaichenko Full Database Backup'
end